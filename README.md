# Elsound
Experimental soundboard for Discord, built with Node and [discord.io](https://github.com/izy521/discord.io).

## Usage
### Commands
Below is the current list of commands.

 - `?help` or `?h` - Sends this list of commands to the user's inbox.
 - `?join` or `?j` - Joins the voice channel the user is in.
 - `?leave` or `?l` - Leaves the current voice chanel.
 - `?play <SOUND>` or `?p` - Plays a sound file. This must be a path under the **sounds/** folder.

### Sounds
Sounds can be installed in the **sounds/** folder. They can be organised however you wish, with the only caveat being that *the subfolder/file names cannot contain spaces* (it screws with the command parsing otherwise).

## Setup
*Quick note: "app" here refers to the Discord side of registering an application, whereas "bot" refers to the final code you run that connects to the app.*

### Creating the App
Before running the bot, you need to create an app for it on the Discord website. Login with your account and go to [https://discordapp.com/developers/applications/me](https://discordapp.com/developers/applications/me), and follow the instructions to create an app there. Name it whatever you want and give it a pretty picture.

Once your app is created, make sure you press the **Add App Bot User** button on the app's settings page. Note down the new User Token under the new **App Bot User** section (you may need to press the **Click to Reveal** option).

Finally, invite the new app to your server. This can be done by going to `https://discordapp.com/oauth2/authorize?&client_id=XXXX&scope=bot`, replacing the `XXXX` section with the app's **Client ID** under the **App Details** section.

### Installing
This section covers the installing of the actual bot on your computer.

 1. Make sure you have Node and NPM installed. You also need ffmpeg or avconv in your system's `PATH` to stream audio.
 2. Run `npm install` from this directory.
 3. Copy **settings.example.json** to **settings.json** and fill in the settings (see below).
 4. Run `node .` from this directory.
 5. Load your sounds into the **sounds/** directory.

### Settings
The bot's settings can be changed through **settings.json** before running the bot. Currently these are minimal, but below you can see the options.

 - `bot_token` - Use the App Bot Token you noted down when you created the app.
 - `prefix` - The prefix to use for controlling the bot via text channels. Default is `?`.
