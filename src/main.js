// Dependencies.
var options = require('../settings')

var fs = require('fs')
var Discord = require('discord.io')

// Setup bot.
let client = new Discord.Client({
    autorun: true,
    token: options.bot_token
})

client.on('ready', function (event) {
    console.log("Logged in as %s (%s)", client.username, client.id)
    console.log("Listening for commands...")

    // Listen for commands.
    client.on('message', function(user, user_id, channel_id, message, event) {

        if (! message.startsWith(options.prefix)) return
        console.log("Recieved command: %s", message)

        // Trim the prefix, then filter the message into a command string and arguments array.
        message = message.substring(options.prefix.length)
        var args = message.split(' ')
        var command = args[0]
        args.splice(0, 1)

        // Pass the command through to the right function.
        switch (command) {
            case 'join':
            case 'j':
                let server = client.servers[client.channels[channel_id].guild_id]
                let vox_channel_id = server.members[user_id].voice_channel_id
                Elsound.join(channel_id, vox_channel_id)
                break;
            case 'leave':
            case 'l':
                Elsound.leave(channel_id)
                break;
            case 'play':
            case 'p':
                Elsound.play(args, channel_id)
                break;
            case 'help':
            case 'h':
                Elsound.help(user, user_id, channel_id)
                break;
            default:
                Elsound.error(channel_id, command)
        }
    })

    // Functions - split this off to a separate file!
    var Elsound = {
        // Variables.
        channel: null,
        sound: null,

        // Help message.
        help: function (name, user, channel) {
            Elsound.sendAndLog(channel, `I have sent help to your inbox, ${name}.`)
            client.sendMessage({ to: user, message: `\`# Elsound Help #\`
Below is the current list of commands.

 - \`?help\` or \`?h\` - Sends this list of commands to the user's inbox.
 - \`?join\` or \`?j\` - Joins the voice channel the user is in.
 - \`?leave\` or \`?l\` - Leaves the current voice chanel.
 - \`?play <SOUND>\` or \`?p\` - Plays a sound file. This must be a path under the **sounds/** folder.

Sounds can be installed in the **sounds/** folder. They can be organised however you wish, with the only caveat being that *the subfolder/file names cannot contain spaces* (it screws with the command parsing otherwise).` })
        },

        // Join a voice channel.
        join: function (channel, vox_channel) {
            Elsound.channel = vox_channel

            client.joinVoiceChannel(Elsound.channel, function (err, events) {
                if (err) return Elsound.sendAndLog(channel, err)

                return Elsound.sendAndLog(channel, `**Joined voice channel.**`)
            })
        },

        // Leave a voice channel - the channel argument is only for text channel response.
        leave: function (channel) {
            if (! Elsound.channel) return Elsound.sendAndLog(channel, `Error: Not currently in a voice channel.`)
            client.leaveVoiceChannel(Elsound.channel)
            Elsound.channel = null
            return Elsound.sendAndLog(channel, `**Left voice channel.**`)
        },

        // Play a sound - the channel argument is only for text channel response.
        play: function (args, channel) {
            Elsound.sound = args[0]

            client.getAudioContext(Elsound.channel, function (err, stream) {
                if (err) return console.log(`[${channel}:] ${err}`)

                fs.createReadStream('./sounds/' + Elsound.sound).pipe(stream, { end: false })
                Elsound.sendAndLog(channel, `**Playing "${Elsound.sound}".**`)
            })
        },

        // Error message.
        error: function (target, command) {
            Elsound.sendAndLog(target, `Error: Command \`${command}\` not recognised.`)
        },

        // Wrapper to send messages and log them to console.
        sendAndLog: function (target, message) {
            client.sendMessage({ to: target, message: message })
            console.log(`[${target}:] ${message}`)
        }
    }
})
